package com.springboot.nio.selector;

import lombok.extern.log4j.Log4j2;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;

@Log4j2
public class Server {
    public static void main(String[] args) throws IOException {
        //1.创建selector，管理多个channel
        Selector selector = Selector.open();

        ServerSocketChannel ssc = ServerSocketChannel.open();
        ssc.configureBlocking(false);//非阻塞模式

        //2.建立selector和channel的联系（注册）
        //SelectionKey 事件发生后，通过SelectionKey可以知道事件和哪个channel发生的事件
        SelectionKey sscKey = ssc.register(selector,0,null);
        //只关注accept事件
        sscKey.interestOps(SelectionKey.OP_ACCEPT);
        log.info("register key:{}",sscKey);
        ssc.bind(new InetSocketAddress(8080));

        while(true){
            //3.select 方法，没有事件发生，线程阻塞，有事件，线程才会恢复运行
            selector.select();
            //4.处理事件,selectedKeys内部包含了所有发生的事件
            Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
            while (iterator.hasNext()){
                SelectionKey key = iterator.next();
                //处理key时，要从SelectionKey集合中删除，否则下次处理就会有问题，报空指针异常
                iterator.remove();
                log.info("key:{}",key);
                //5.区分事件类型
                if (key.isAcceptable()) {
                    ServerSocketChannel channel = (ServerSocketChannel) key.channel();
                    SocketChannel sc = channel.accept();
                    sc.configureBlocking(false);
                    SelectionKey scKey = sc.register(selector, 0, null);
                    //关注读事件
                    scKey.interestOps(SelectionKey.OP_READ);
                    log.info("{}",sc);
                } else if (key.isReadable()){
                    try {
                        SocketChannel channel = (SocketChannel) key.channel();//拿到触发事件的channel
                        ByteBuffer buffer = ByteBuffer.allocate(16);
                        //如果是正常断开，read的方法返回值是-1
                        int read = channel.read(buffer);
                        if (read == -1) {
                            key.cancel();
                        } else {
                            buffer.flip();
                            log.info(StandardCharsets.UTF_8.decode(buffer).toString());
                        }
                    }catch (IOException e) {
                        e.printStackTrace();
                        //因为客户端断开了，因此需要将key取消（从selector的key集合中删除）
                        key.cancel();;
                    }
                }
            }
        }
    }
}
