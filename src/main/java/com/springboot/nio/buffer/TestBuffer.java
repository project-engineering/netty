package com.springboot.nio.buffer;

import lombok.extern.log4j.Log4j2;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * ByteBuffer正确使用姿势
 * 1.向buffer写入数据，例如调用channel.read(buffer)
 * 2.调用flip()切换至读模式
 * 3.从buffer读取数据，例如调用buffer.get()
 * 4.调用clear()或者compact()切换至写模式
 * 5.重复1-4步骤
 */
@Log4j2
public class TestBuffer {
    public static void main(String[] args) {
        //FileChannel
        //1.输入输出流  2.RandomAccessFile
        try (FileChannel channel = new FileInputStream("data.txt").getChannel()) {
            //准备缓冲区
            ByteBuffer byteBuffer = ByteBuffer.allocate(10);
            while(true){
                int lenght = channel.read(byteBuffer);
                log.info("读取到的字节数{}",lenght);
                //当lenght为-1时，表示没有剩余内容了
                if (lenght == -1) {
                    break;
                }
                //切换为读模式
                byteBuffer.flip();
                //是否还有剩余未读取数据
                while(byteBuffer.hasRemaining()){
                    byte b = byteBuffer.get();
                    log.info("实际字节{}",b);
                }
                //切换为写模式
                byteBuffer.clear();
            }
        } catch (IOException e) {

        }
    }
}
