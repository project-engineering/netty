package com.springboot.nio.buffer;

import lombok.extern.log4j.Log4j2;

import java.nio.ByteBuffer;

@Log4j2
public class TestByteBufferAllocate {
    public static void main(String[] args) {
        /**
         * class java.nio.HeapByteBuffer -java堆内存，读写效率较低，受到GC的影响
         */
        log.info(ByteBuffer.allocate(16).getClass());
        /**
         * class java.nio.DirectByteBuffer -直接内存，读写效率高（少一次拷贝），不会受GC影响，
         * 但是分配效率低，如果使用不当，未释放完全，会造成内存泄漏
         */
        log.info(ByteBuffer.allocateDirect(16).getClass());

    }
}
