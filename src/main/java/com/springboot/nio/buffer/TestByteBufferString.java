package com.springboot.nio.buffer;

import lombok.extern.log4j.Log4j2;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.StandardCharsets;

@Log4j2
public class TestByteBufferString {
    public static void main(String[] args) {
        //1.字符串转为ByteBuffer
        ByteBuffer byteBuffer = ByteBuffer.allocate(16);
        byteBuffer.put("hello".getBytes());
        //2.Charset
        ByteBuffer buffer = StandardCharsets.UTF_8.encode("你好");
        CharBuffer buffer1 = StandardCharsets.UTF_8.decode(buffer);
        log.info(buffer1.toString());
    }
}
