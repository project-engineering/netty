package com.springboot.nio.buffer;

import lombok.extern.log4j.Log4j2;

import java.nio.ByteBuffer;

@Log4j2
public class TestByteBufferRead {
    public static void main(String[] args) {
       /**
         * 该代码展示了如何使用ByteBuffer类读取数据。
         * 首先，创建一个大小为10的ByteBuffer对象。
         * 然后，将字节数组{'a', 'b', 'c', 'd'}放入缓冲区。
         * 接下来，调用flip()方法将缓冲区从写模式切换到读模式。
         * 使用get()方法读取4个字节的数据，并打印缓冲区的位置。
         * 最后，使用rewind()方法将缓冲区的位置重置为0，并再次使用get()方法读取一个字节的数据，并将其转换为字符后打印出来。
         */
        ByteBuffer buffer = ByteBuffer.allocate(10);
        buffer.put(new byte[]{'a', 'b', 'c', 'd'});
        buffer.flip();
        buffer.get(new byte[4]);
        log.info(buffer.position());
        buffer.rewind();
        log.info((char) buffer.get());

        //mark做一个标记，记录position位置，reset是将position重置到mark的位置
        log.info((char) buffer.get());
        buffer.mark();//加标记，目前索引的位置为1，对应的值是b
        //继续读，将c和d都读取出来
        log.info((char) buffer.get());
        log.info((char) buffer.get());
        //将索引position重置到mark的位置
        log.info("重置索引position到mark位置。。。"+buffer.reset());
        log.info((char) buffer.get());
        log.info((char) buffer.get());
    }
}
