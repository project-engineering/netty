package com.springboot.nio.files;

import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.atomic.AtomicInteger;

@Log4j2
public class TestFilesWalkFileTree {
    public static void main(String[] args) throws IOException {
        //遍历删除  “D:\tools\Java\jdk-17 - 副本”  文件夹及该文件夹下的所有文件及文件夹
        Files.walkFileTree(Paths.get("D:\\tools\\Java\\jdk-17 - 副本"),new SimpleFileVisitor<Path>(){
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                log.info("进入文件夹{}",dir);
                return super.preVisitDirectory(dir, attrs);
            }

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                log.info("开始删除文件{}",file);
                Files.delete(file);
                log.info("删除文件{}成功",file);
                return super.visitFile(file, attrs);
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                log.info("开始删除文件夹{}",dir);
                Files.delete(dir);
                log.info("删除文件夹{}成功",dir);
                return super.postVisitDirectory(dir, exc);
            }

            @Override
            public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                log.info("访问文件{}失败，"+exc.getMessage(),file);
                return super.visitFileFailed(file, exc);
            }
        });
    }

    private static void m1 () throws IOException{
        AtomicInteger dirCount = new AtomicInteger();
        AtomicInteger fileCount = new AtomicInteger();
        Files.walkFileTree(Paths.get("D:\\tools\\Java\\jdk-17"),new SimpleFileVisitor<Path>(){
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                log.info("===>"+dir);
                dirCount.incrementAndGet();
                return super.preVisitDirectory(dir, attrs);
            }

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                log.info(file);
                fileCount.incrementAndGet();
                return super.visitFile(file, attrs);
            }
        });
        log.info("dir count:"+dirCount);
        log.info("file count:"+fileCount);
    }

    private static void m2 () throws IOException{
        AtomicInteger jarCount = new AtomicInteger();
        Files.walkFileTree(Paths.get("D:\\tools\\Java\\jdk-17"),new SimpleFileVisitor<Path>(){
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                if (file.toString().endsWith(".jar")) {
                    log.info(file);
                    jarCount.incrementAndGet();
                }
                return super.visitFile(file, attrs);
            }
        });
        log.info("jar count:"+jarCount);
    }
}
