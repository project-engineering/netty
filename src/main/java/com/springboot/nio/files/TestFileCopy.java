package com.springboot.nio.files;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * NIO多级拷贝测试
 */
public class TestFileCopy {
    public static void main(String[] args) throws IOException {
        String source = "D:\\tools\\Java\\jdk-17";
        String target = "D:\\tools\\Java\\jdk-17 - 副本";
        Files.walk(Paths.get(source)).forEach(path -> {
            try {
                String targetName = path.toString().replace(source,target);
                //如果是目录
                if (Files.isDirectory(path)) {
                    Files.createDirectory(Paths.get(targetName));
                } else if (Files.isRegularFile(path)){
                    //普通文件
                    Files.copy(path,Paths.get(targetName));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
