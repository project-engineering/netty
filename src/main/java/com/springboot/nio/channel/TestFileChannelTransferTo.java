package com.springboot.nio.channel;

import lombok.extern.log4j.Log4j2;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

@Log4j2
public class TestFileChannelTransferTo {
    public static void main(String[] args) {
       /**
        * 这段代码从一个文件将数据传输到另一个文件。
        */
        try {
            FileChannel from = new FileInputStream("data.txt").getChannel();
            FileChannel to = new FileOutputStream("to.txt").getChannel();
            //效率高，底层会利用操作系统的零拷贝进行优化，但是一次只能传输2G的数据，可以进行如下优化
            long size = from.size();
            // left 变量代表还剩余多少字节
            for (long left = size; left >0 ;){
                log.info("position："+(size-left) + " left："+left);
                left -= from.transferTo((size - left), left , to);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
